===============================================================================
[VAGRANT][ANSIBLE] Déploiement de Wordpress avec base de données distante Mysql
===============================================================================

Ce projet déploie :

- 1 VM avec une base de données Mysql
- 1 VM avec un serveur Wordpress


Requirements
------------

Nécessite l'installation de Vagrant et de Ansible.

.. code:: bash

    $ sudo apt-get update
    $ sudo apt-get install vagrant
    $ sudo apt-get install ansible
    
Nécessite l'installation de Virtualbox.

.. code:: bash

    $ echo "deb http://download.virtualbox.org/virtualbox/debian stretch contrib" >> /etc/apt/sources.list
    $ wget -q -O- https://www.virtualbox.org/download/oracle_vbox_2016.asc | apt-key add
    $ sudo apt-get update
    $ sudo apt-get install virtualbox-5.1

Déploiement
-----------
    
Clonage du repository.

.. code:: bash

    $ git clone https://gitlab.com/jordan.tan/wordpress_vagrant_ansible && cd wordpress_vagrant_ansible

Création des machines virtuelles avec Vagrant puis déploiement de Wordpress/Mysql avec Ansible.

.. code:: bash

    $ vagrant up

Utilisation
-----------

On se connecte à la VM du serveur Wordpress puis on lance firefox :

.. code:: bash

    $ vagrant ssh server
    $ firefox 192.168.1.254

